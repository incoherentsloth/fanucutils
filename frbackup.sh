#/bin/sh 

function print_usage() {
    printf 1>&2 "Usage:\n"
    printf 1>&2 "$0 [-f] [-h] address dest\n\n"
    printf 1>&2 "  -f       Force directory overwrite\n"
    printf 1>&2 "  -h       Print this help\n"
    printf 1>&2 "  address  Robot address\n"
    printf 1>&2 "  dest     Name of backup directory\n"
}

while  getopts :fh opt ; do 
    case $opt in
        f)
            overwrite=1
            ;;

        h) 
            print_usage
            exit 0
            ;;

        [?])
            printf 1>&2 "$0: Invalid option $1\n"
            print_usage
            exit 1
            ;;
    esac
done

shift $(($OPTIND - 1))
if [ $# -lt 2 ]; then 
    printf 1>&2 "$0: Missing arguments"
    print_usage
    exit 1
elif [ $# -gt 2 ]; then 
    printf 1>&2 "$0; Too many arguments"
    print_usage
    exit 1
fi

rbtaddr=$1
destdir=$2

if [ ! -z ${overwrite+x} ] && [ -d $destdir ]; then 
    rm -rf $destdir/*
fi

if [ -d $destdir ] && [ ! -z "$(ls -A $destdir)" ]; then 
    printf 1>&2 "$0: Destination directory not empty\n"
    exit 1
fi

[ -d $destdir ] || mkdir $destdir

cd $destdir

# FANUC Robot controller ftp server doesn't switch automatically to binary
# during a transfer of a binary file, also they stick with MS-DOS path format:
# they use resources (MD:\ MDB:\ ...) and back slash. During a ftp connection
# the controller switch to lowercase names (if you perform a USB backup with an
# active FTP connection all the file names will be lowecase), seems that 
# controller runs on a sort of FAT or case insensitive flavored disk format
# 
# A backup of the controller is achieved downloading the entire content of
# resource (note the backslash instead of normal slash):
#   MDB:\   Memory Disk Binary 
#
# Other resources are present: 
#   MD:\    Memory Disk
#   MC:\    Memory Card
#   FR:\    FROM Memory     Flash ROM
#   UD1:\   User Disk 1     USB connected to cabinet port
#   UT1:\   User disk TP 1  USB connected to TP port

ftp -nv $rbtaddr <<END_SCRIPT
quote USER anonymous
quote PASS guest
binary
prompt
quote CWD mdb:
mget .\*
quote QUIT
END_SCRIPT

exit 0

